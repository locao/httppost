#ifndef HTTPPOST_H
#define HTTPPOST_H

#include <map>
#include <sstream>
#include <string>
#include <winsock2.h>

#pragma once
class HTTPPost
{
public:
	HTTPPost(void);
	~HTTPPost(void);

	void setDestination(std::string hostname);
	std::string getDestination() const;

	void setHost(std::string hostname);
	std::string getHost() const;
	
	void addParameterAndValue(const std::string parameterName, const std::string parameterValue);

	std::string postAndGetAnswer();

private:
	std::string destinationCall_;
	std::string host_;
	std::map<std::string, std::string> parametersAndValues_;
	std::string postContent_;
	std::ostringstream postHeader_;
	WSADATA wsaData_;
	int socketFileDescriptor_;
	struct addrinfo  socketTypeWeSupport_;
	struct addrinfo* hostInformation_;
	struct addrinfo* connectedHostInformation_;

	void prepareSocket_();
	bool isSocketReady_();
	bool isWinSockInitiated_();
	void initiateWinSock_();
	void initiateClientSocketCapabilities_();
	void initiateHostInformation_();
	bool connectToHost_();
	void preparePostContent_();
	void preparePostHeader_();
	
};

#endif