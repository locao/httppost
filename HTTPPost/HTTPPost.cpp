#include "stdafx.h"
#include "HTTPPost.h"

#include <Ws2tcpip.h>

#pragma comment(lib, "Ws2_32.lib")

const int SUCCESS = 0;
const int BUFFER_SIZE = 512;

HTTPPost::HTTPPost(void)
{
	hostInformation_ = NULL;
}


HTTPPost::~HTTPPost(void)
{
}

void HTTPPost::setHost(std::string hostname)
{
	host_ = hostname;
}

std::string HTTPPost::getHost() const
{
	return host_;
}

void HTTPPost::setDestination(std::string destination)
{
	destinationCall_ = destination;
}

std::string HTTPPost::getDestination() const
{
	return destinationCall_;
}

void HTTPPost::addParameterAndValue(const std::string parameterName, const std::string parameterValue)
{
	parametersAndValues_[parameterName] = parameterValue;
}

std::string HTTPPost::postAndGetAnswer()
{
	std::string returnString;
	char receiveBuffer[BUFFER_SIZE];
	int bytesReceived;

	prepareSocket_();
	if(connectToHost_() == false)
		throw std::exception("Failed to connect to host");

	preparePostContent_();
	preparePostHeader_();

    const auto sendBuffer = postHeader_.str() + postContent_;

	send(socketFileDescriptor_, sendBuffer.data(), sendBuffer.size(), NULL);
	do {
		bytesReceived = recv(socketFileDescriptor_, receiveBuffer, BUFFER_SIZE, 0);
        if(bytesReceived < 0)
			throw std::exception("Failed receiving answer");
    } while( bytesReceived > 0 );
    
	returnString = receiveBuffer;
	
	freeaddrinfo(hostInformation_);
	hostInformation_ = 0;

	return returnString;
}

void HTTPPost::prepareSocket_()
{
	initiateWinSock_();
	initiateClientSocketCapabilities_();
	initiateHostInformation_();
}

bool HTTPPost::isWinSockInitiated_()
{
	if((LOBYTE(wsaData_.wVersion) == 2) && (HIBYTE(wsaData_.wVersion) == 2))
		return true;

	return false;
}

void HTTPPost::initiateWinSock_()
{
	if(isWinSockInitiated_())
		return;

	if(WSAStartup(MAKEWORD(2,2), &wsaData_) != SUCCESS)
		throw std::exception("Failed to initiate WinSock");
}

void HTTPPost::initiateClientSocketCapabilities_()
{
	if(socketTypeWeSupport_.ai_socktype != SOCK_STREAM) {
		memset(&socketTypeWeSupport_, 0, sizeof(socketTypeWeSupport_));
		socketTypeWeSupport_.ai_family = AF_UNSPEC;
		socketTypeWeSupport_.ai_socktype = SOCK_STREAM;
	}
}

void HTTPPost::initiateHostInformation_()
{
	if(hostInformation_ != NULL)
		return;

	if (getaddrinfo(host_.c_str(), "http", &socketTypeWeSupport_, &hostInformation_) != SUCCESS)
		throw std::exception("Failed to initiate host information");	
}

bool HTTPPost::connectToHost_()
{
	// hostInformation_ is the head of a linked list of host information structures. We'll iterate through it trying 
	// to connect
	for(connectedHostInformation_ = hostInformation_; 
		connectedHostInformation_ != NULL; 
		connectedHostInformation_ = connectedHostInformation_->ai_next) {	
			socketFileDescriptor_ = socket(connectedHostInformation_->ai_family, 
				connectedHostInformation_->ai_socktype, 
				connectedHostInformation_->ai_protocol);
			if(socketFileDescriptor_ == -1)
				continue;
			
			if (connect(socketFileDescriptor_, connectedHostInformation_->ai_addr, connectedHostInformation_->ai_addrlen) == -1) {
				closesocket(socketFileDescriptor_);
				continue;
			}
			return true;
	}
	return false;
}

void HTTPPost::preparePostContent_()
{
	postContent_ = "";
	
	for(auto pair: parametersAndValues_) {
		if(postContent_.length() > 0)
			postContent_ += "&";
		postContent_ += pair.first + "=" + pair.second;
	}
}

void HTTPPost::preparePostHeader_()
{
	postHeader_ << "POST " + destinationCall_ + " HTTP/1.1\n";
    postHeader_ << "Content-Type: application/x-www-form-urlencoded\n";
    postHeader_ << "Host: " + host_ + "\n";
    postHeader_ << "Content-Length: " << std::to_string(postContent_.length()) << "\n\n";
}
