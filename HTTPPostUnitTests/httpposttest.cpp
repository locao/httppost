#include "stdafx.h"
#include "CppUnitTest.h"
#include <HTTPPost.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace HTTPPostUnitTests
{		
	TEST_CLASS(UnitTestHTTPPost)
	{
	public:
		TEST_METHOD(TestSetAndGetHost)
		{
			HTTPPost post;
			std::string host;

			host = "posttestserver.com";

			post.setHost(host);
			Assert::IsTrue(post.getHost() == host);
		}

		TEST_METHOD(TestSetAndGetDestination)
		{
			HTTPPost post;
			std::string destination;

			destination = "/post.php";

			post.setDestination(destination);
			Assert::IsTrue(post.getDestination() == destination);
		}

		TEST_METHOD(TestPost)
		{
			HTTPPost post;
			std::string host;
			std::string destination;
			std::string parameterName;
			std::string parameterValue;
			std::string answer;

			host = "posttestserver.com";
			destination = "/post.php";
			parameterName = "ThisIsAParameterName";
			parameterValue = "AndThisIsItsValue";

			post.setHost(host);
			post.setDestination(destination);
			post.addParameterAndValue(parameterName, parameterValue);

			answer = post.postAndGetAnswer();

			Assert::IsTrue(answer.length() > 0);
			Assert::IsTrue(answer.find("HTTP/1.1 200 OK") != std::string::npos);
			Assert::IsTrue(answer.find("Successfully dumped 1 post variable") != std::string::npos);			
		}

		TEST_METHOD(TestPostTwoVariables)
		{
			HTTPPost post;
			std::string host;
			std::string destination;
			std::string parameterFirstParameterName;
			std::string parameterFirstParameterValue;
			std::string parameterSecondParameterName;
			std::string parameterSecondParameterValue;
			std::string answer;

			host = "posttestserver.com";
			destination = "/post.php";
			parameterFirstParameterName = "OneParameter";
			parameterFirstParameterValue = "HasOneValue";
			parameterSecondParameterName = "Other parameter";
			parameterSecondParameterValue = "has three values";

			post.setHost(host);
			post.setDestination(destination);
			post.addParameterAndValue(parameterFirstParameterName, parameterFirstParameterValue);
			post.addParameterAndValue(parameterSecondParameterName, parameterSecondParameterValue);

			answer = post.postAndGetAnswer();

			Assert::IsTrue(answer.length() > 0);
			Assert::IsTrue(answer.find("HTTP/1.1 200 OK") != std::string::npos);
			Assert::IsTrue(answer.find("Successfully dumped 2 post variables") != std::string::npos);			
		}

		TEST_METHOD(TestNoVariablesPost)
		{
			HTTPPost post;
			std::string host;
			std::string destination;
			std::string parameterName;
			std::string parameterValue;
			std::string answer;

			host = "posttestserver.com";
			destination = "/post.php";

			post.setHost(host);
			post.setDestination(destination);

			answer = post.postAndGetAnswer();

			Assert::IsTrue(answer.length() > 0);
			Assert::IsTrue(answer.find("HTTP/1.1 200 OK") != std::string::npos);
			Assert::IsTrue(answer.find("Successfully dumped 0 post variable") != std::string::npos);			
		}

		TEST_METHOD(TestInvalidHost)
		{
			HTTPPost post;
			std::string host;
			std::string destination;
			std::string parameterName;
			std::string parameterValue;
			std::string answer;

			host = "nioewkmcdioslkn.com";
			destination = "/post.php";

			post.setHost(host);
			post.setDestination(destination);
					
			Assert::ExpectException<std::exception>([&] { post.postAndGetAnswer(); });
		}
	};
}